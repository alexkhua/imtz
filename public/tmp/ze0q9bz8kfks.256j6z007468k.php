<div class="container">
    <div class="row">

        <div class="col-md-2 col-sm-6 col-xs-6">
            <div class="left-sidebar">
                <h3>Почта</h3>
                <a href="/" type="button" class="btn btn-default">
                    Входящие
                </a>
                <a href="/sent" type="button" class="btn btn-default active">
                    Отправленные
                </a>
            </div>
        </div>

        <div class="col-md-10 col-sm-12 col-xs-12">
            <div class="mailbox">
                <a href="#" type="button" class="btn btn-default" data-toggle="modal" data-target="#modalMessage">
                    Написать письмо
                </a>
                <a href="#" id="deleteSentEmail" type="button" class="btn btn-default">
                    Удалить выбранные письма
                </a>
                <div id="sentEmailListContainer" class="table-responsive">

                </div>
            </div>
        </div>
    </div>
</div>