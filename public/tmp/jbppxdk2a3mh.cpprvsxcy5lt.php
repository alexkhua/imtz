<table class="table table-bordered" id="emailList">
    <thead>
    <tr>
        <th data-sorter="false"><input name="selectAll" type="checkbox" id="selectAll"></th>
        <th>Отправитель</th>
        <th data-sorter="false">Тема письма</th>
        <th>Дата получения</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach (($receivedEmails?:[]) as $receivedEmail): ?>
        <tr id="<?= ($receivedEmail['id']) ?>">
            <td><input name="receivedEmailId[]" value="<?= ($receivedEmail['id']) ?>" type="checkbox"></td>
            <td><a href="#" class="mailto"><?= ($receivedEmail['from']) ?></a></td>
            <td><a href="#" data-toggle="modal" data-target="#emailBox<?= ($receivedEmail['id']) ?>"><?= ($receivedEmail['subject']) ?></a></td>
            <td><?= ($receivedEmail['date']) ?></td>
            <td id="emailBox<?= ($receivedEmail['id']) ?>" class="modal fade">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <?= ($receivedEmail['message'])."
" ?>
                        <div class="text-right">
                            <button class="btn btn-default no-margin-bottom" type="submit" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>