<table class="table table-bordered" id="emailList">
    <thead>
    <tr>
        <th data-sorter="false"><input name="selectAll" type="checkbox" id="selectAll"></th>
        <th>Получатель</th>
        <th data-sorter="false">Тема письма</th>
        <th>Дата отправки</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach (($sentEmails?:[]) as $sentEmail): ?>
        <tr id="<?= ($sentEmail['id']) ?>">
            <td><input name="sentEmailId[]" value="<?= ($sentEmail['id']) ?>" type="checkbox"></td>
            <td><a href="#" class="mailto"><?= ($sentEmail['recipient']) ?></a></td>
            <td><a href="#" data-toggle="modal" data-target="#emailBox<?= ($sentEmail['id']) ?>"><?= ($sentEmail['subject']) ?></a></td>
            <td><?= ($sentEmail['date']) ?></td>
            <td id="emailBox<?= ($sentEmail['id']) ?>" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <?= ($sentEmail['message'])."
" ?>
                        <div class="text-right">
                            <button class="btn btn-default no-margin-bottom" type="submit" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>