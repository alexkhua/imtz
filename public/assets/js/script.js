$( document ).ready(function() {

    $(document).ajaxSuccess(function() {
        $('#selectAll').change(function() {
            var checkboxes = $(this).closest('table').find(':checkbox');
            checkboxes.prop('checked', $(this).is(':checked'));
        });
        $('.mailto').click(function(){
            email = $(this).text();
            $('#sendmessage input[name=to]').val(email);
            $('#modalMessage').modal();
        });
    });

    $("#sendmessage").submit(function(e) {

        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            beforeSend: function(){
                spinnerStart();
                $('#modalMessage #sendMsg').attr('disabled', 'disabled');
                $('#sendMessageResult').empty();
            },
            success: function(data){
                if($("#sentEmailListContainer").length > 0){
                    $("#sentEmailListContainer").empty();
                    getSentEmails();
                    $("#emailList").trigger('update');
                }
                $('#modalMessage #sendMsg').removeAttr('disabled');
                spinnerStop();
                $('#sendmessage')[0].reset();
                messageHtml =
                    '<div class="alert alert-success fade in">\n' +
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>\n' +
                    data
                    +'</div>';
                addMessageToContainer('#sendMessageResult', messageHtml);
            },
            error: function(e){
                $('#modalMessage #sendMsg').removeAttr('disabled');
                spinnerStop();
                switch(e.status){
                    case 500: message = 'Ошибка отправки почты'; break;
                    case 501: message = 'Указан не допустимый Email'; break;
                    case  401: message = 'Ошибка авторизации'; break;
                }
                messageHtml = '<div class="alert alert-danger fade in">\n' +
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>\n' +
                    message
                    +'</div>';

                addMessageToContainer('#sendMessageResult', messageHtml);
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });

    $('#deleteSentEmail').click(function(){
        $( "#selectAll" ).prop( "checked", false );
        data = $('#emailList input[name*=sentEmailId]').serialize();
        if(data.length <= 0) return false;
        $.ajax({
            type: "POST",
            url: '/deleteSentEmail',
            data: data,
            beforeSend: function(){
                spinnerStart();
            },
            error: function(){
                spinnerStop();
            },
            success: function(data){
                spinnerStop();
                $('#emailList input:checked').each(function(){
                    $('#emailList #' +this.value).remove();
                    $('#emailList').trigger('update');
                });
            }

        });
    });

    $('#deleteReceivedEmail').click(function(){
        $( "#selectAll" ).prop( "checked", false );
        data = $('#emailList input[name*=receivedEmailId]').serialize();
        if(data.length <= 0) return false;
        $.ajax({
            type: "POST",
            url: '/deleteReceivedEmail',
            data: data,
            beforeSend: function(){
                backdropStart();
                spinnerStart();
            },
            error: function(){
                backdropStop();
                spinnerStop();
            },
            success: function(data){
                spinnerStop();
                $(".modal-backdrop").remove();
                $('#emailList input:checked').each(function(){
                    $('#emailList #' +this.value).remove();
                    $('#emailList').trigger('update');
                });
            }

        });
    });

    function getSentEmails(){
        $.ajax({
            type: "POST",
            url: '/getSentEmails',
            beforeSend: function(){
                spinnerStart();
            },
            error: function(){
                spinnerStop();
                alrt('Ошибка загрузки данных');
            },
            success: function(data){
                spinnerStop();
                $(".modal-backdrop").remove();
                $('#sentEmailListContainer').append(data);
                $("#emailList").tablesorter();
            }

        });
    }

    function getReceivedEmails(){
        $.ajax({
            type: "POST",
            url: '/getReceivedEmails',
            beforeSend: function(){
                spinnerStart();
                backdropStart();
            },
            error: function(){
                spinnerStop();
                backdropStop();
                alert('Ошибка загрузки данных');
            },
            success: function(data){
                spinnerStop();
                backdropStop();
                $('#receivedEmailListContainer').append(data);
                $("#emailList").tablesorter();
            }

        });
    }

    if($("#sentEmailListContainer").length > 0){
        getSentEmails();
    }else if($("#receivedEmailListContainer").length > 0){
        getReceivedEmails();
    }

    function spinnerStart()
    {
        $('.loading').fadeIn();
        $('#modalMessage').css('z-index', '1');
    }

    function spinnerStop()
    {
        $('.loading').fadeOut();
        $('#modalMessage').css('z-index', '1050');
    }


    function backdropStart(){
        if($('.modal-backdrop').length == 0){
            $('<div class="modal-backdrop in"></div>').appendTo(document.body);
        }
    }

    function backdropStop(){
        $(".modal-backdrop").remove();
    }

    function addMessageToContainer(containerId, messageHtml)
    {
        $(containerId).append(messageHtml);
    }

});

