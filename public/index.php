<?php

// Kickstart the framework

require('../vendor/autoload.php');

$f3 = Base::instance();

// Load configuration
$f3->config('../app/config.ini');

$f3->route('GET /','IndexController->index');
$f3->route('GET /sent','IndexController->sent');

$f3->route('POST /getSentEmails','AjaxController->getSentEmails');
$f3->route('POST /getReceivedEmails','AjaxController->getReceivedEmails');

$f3->route('POST /sendmessage','AjaxController->sendEmail');
$f3->route('POST /deleteSentEmail','AjaxController->deleteSentEmail');
$f3->route('POST /deleteReceivedEmail','AjaxController->deleteReceivedEmail');

$f3->route('GET /auth','UserController->index');
$f3->route('POST /auth','UserController->auth');


$f3->run();
