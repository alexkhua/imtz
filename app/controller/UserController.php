<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 28.08.2018
 * Time: 13:42
 */

/**
 * Class UserController
 */
class UserController extends BaseController
{

    /**
     * Render authorization page view template
     */
    public function index()
    {
        $this->f3->clear('SESSION.isAuth');
        $this->f3->clear('SESSION.username');
        $this->f3->set('view', 'auth.html');
        echo $this->template->render('layout.html');
    }

    /**
     * Check authorization
     */
    public function auth()
    {
        $isAuth = false;

        $username = $this->f3->get('POST.username');
        $password = $this->f3->get('POST.password');

        $user = new UserModel($this->db);
        $user->getByName($username);

        if (!$user->dry()) {
            if(password_verify ($password, $user->password)){
                $isAuth = true;
            }
        }

        if(!$isAuth){
            $this->f3->set('authError', true);
            $this->f3->set('view', 'auth.html');
            echo $this->template->render('layout.html');
            exit();
        }

        $this->f3->set('SESSION.isAuth', true);
        $this->f3->set('SESSION.userId', $user->id);
        $this->f3->set('SESSION.username', $username);
        $this->f3->reroute('/');

    }

    /**
     * Add new user. Not used
     */
    public function add()
    {
        $user = new UserModel($this->db);

        $username = $this->f3->get('POST.username');
        $password = password_hash($this->f3->get('POST.password'), PASSWORD_DEFAULT);

        $user->add($username, $password);
        $this->f3->reroute('/auth');
    }

    /**
     * Need redefine to prevent redirects, becouse class inherited from base controller
     */
    public function beforeroute()
    {

    }


}