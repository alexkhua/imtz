<?php

/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 28.08.2018
 * Time: 20:13
 */

use SSilence\ImapClient\ImapClientException;
use SSilence\ImapClient\ImapConnect;
use SSilence\ImapClient\ImapClient as Imap;

/**
 * Class AjaxController
 */
class AjaxController extends BaseController
{
    protected $smtp;

    /**
     *  Render received emails view template
     *  @return void
     */

    public function getReceivedEmails()
    {
        $imap = $this->imapConnect();				
        $messages = $imap->getMessages();

        $receivedEmails = [];

        foreach($messages as $msg) {
            $date = date_parse_from_format("D, d M Y H:i:s O T", $msg->header->date);
            $timestamp = mktime(
                $date['hour'],
                $date['minute'],
                $date['second'],
                $date['month'],
                $date['day'],
                $date['year']
            );
            $date    = date('Y-m-d H:i:s', $timestamp);
            $message = $msg->message->html ? $message = $msg->message->html : $msg->message->plain;
            $receivedEmails[] = [
                'from' => $msg->header->from,
                'subject' => $msg->header->subject,
                'id' =>  $msg->header->uid,
                'date' => $date,
                'message' =>  $message,
            ];
        }

        $this->f3->set('receivedEmails', $receivedEmails);;
        echo $this->template->render('ajax/receivedEmails.html');
    }

    /**
     * Delete received emails
     * @return void
     */

    public function deleteReceivedEmail()
    {
        $emailsId = $this->f3->get('POST.receivedEmailId');
        if(is_array($emailsId) && !empty($emailsId)) {
            $imap = $this->imapConnect();
            foreach ($emailsId as $emailId) {
                $imap->deleteMessage($emailId);
            }
        }
    }

    /**
     * Sent email
     * @return void
     */

    public function sendEmail()
    {
        $to = trim($this->f3->get('POST.to'));
        $subject = filter_var ($this->f3->get('POST.subject'), FILTER_SANITIZE_STRING);
        $message = filter_var($this->f3->get('POST.message'), FILTER_SANITIZE_STRING);

        if (!filter_var($to, FILTER_VALIDATE_EMAIL)) {
            $this->f3->error(501, "Email is not valid");
            exit();
        }

        $this->smtp->set('To', $to);
        $this->smtp->set('From', $this->f3->get('smtpusername'));
        $this->smtp->set('Subject', $subject);
        if($this->smtp->send($message)){
            $mailModel = new MailModel($this->db);
            $mailModel->addSent($to, $subject, $message);
            echo 'Сообщение успешно отправлено!';
        }else{
            $this->f3->error(500, "Internal Server Error");
        }
    }

    /**
     * Render sent emails view template
     * @return void
     */
    public function getSentEmails()
    {
        $mailModel = new MailModel($this->db);
        $sentEmails = $mailModel->all();
        $this->f3->set('sentEmails', $sentEmails);
        echo $this->template->render('ajax/sentEmails.html');
    }

    /**
     * Delete sent email
     * @return void
     */
    public function deleteSentEmail()
    {
        $emailsId = $this->f3->get('POST.sentEmailId');
        if(is_array($emailsId) && !empty($emailsId)) {
            $mailModel = new MailModel($this->db);
            foreach ($emailsId as $emailId) {
                $mailModel->delete((int)$emailId);
            }
        }
    }

    /**
     * Imap
     * @return Imap handler
     */
    public function imapConnect()
		{
				$mailbox = $this->f3->get('smtphost');
				$username = $this->f3->get('smtpusername');
				$password = $this->f3->get('smtppassword');
				$encryption = Imap::ENCRYPT_SSL;

				try{
						$imap = new Imap($mailbox, $username, $password, $encryption);

				}catch (ImapClientException $error){
						echo $error->getMessage().PHP_EOL;
						die();
				}
				return $imap;
		}

    /**
     * If user not authorized return error 401
     */
    public function beforeroute()
    {
        if(!$this->isAuth())
        {
            $this->f3->error(401, "Unauthorized");
            exit();
        }
    }

    /**
     * AjaxController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->smtp = new SMTP (
            $this->f3->get('smtphost'),
            $this->f3->get('smtpport'),
            $this->f3->get('smtpscheme'),
            $this->f3->get('smtpusername'),
            $this->f3->get('smtppassword')
        );
    }
}