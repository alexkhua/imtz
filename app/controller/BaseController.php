<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 28.08.2018
 * Time: 13:20
 */

/**
 * Class BaseController
 */
class BaseController
{
    protected $f3;
    protected $db;
    protected $template;

    /**
     * Check authorization
     * @return bool
     */
    public function isAuth()
    {
        if ($this->f3->get('SESSION.isAuth')){
            return true;
        }
    }

    /**
     * By default, redirect to the authorization page if user not authorized
     */
    public function beforeroute()
    {
        if(!$this->isAuth()){
            $this->f3->reroute('/auth');
            exit();
        }
    }

    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        $this->f3 = Base::instance();
        $db=new DB\SQL(
            $this->f3->get('devdb'),
            $this->f3->get('devdbusername'),
            $this->f3->get('devdbpassword'),
            array( \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION )
        );
        $this->db=$db;
        $this->template = new Template();
    }
}