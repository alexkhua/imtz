<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 30.08.2018
 * Time: 9:58
 */

/**
 * Class IndexController
 */

class IndexController extends BaseController
{
    /**
     * Render index page view template
     */
    public function index()
    {
        $this->f3->set('view', 'index.html');
        echo $this->template->render('layout.html');
    }

    /**
     * Render sent page view template
     */
    public function sent()
    {
        $this->f3->set('view', 'sent.html');
        echo $this->template->render('layout.html');
    }
}