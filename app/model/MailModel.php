<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 29.08.2018
 * Time: 16:45
 */

/**
 * Class MailModel
 */
class MailModel extends DB\SQL\Mapper
{
    protected $f3;

    /**
     * MailModel constructor.
     * @param \DB\SQL $db
     */
    public function __construct(DB\SQL $db)
    {
        parent::__construct($db, 'sent_messages');
        $this->f3 = \Base::instance();
    }

    /**
     * Get all sent emails
     * @return array
     */
    public function all()
    {
        $this->load(['owner_id=?', $this->f3->get('SESSION.userId')], ['order' => 'date DESC',]);
        return $this->query;
    }

    /**
     * Add sent email
     * @param $to
     * @param $subject
     * @param $message
     */
    public function addSent($to, $subject, $message)
    {
        $this->set('owner_id', $this->f3->get('SESSION.userId'));
        $this->set('recipient', $to);
        $this->set('subject', $subject);
        $this->set('message', $message);
        $this->save();
    }

    /**
     * Delete sent email
     * @param $id
     */
    public function delete($id)
    {
        $this->load(['id=? and owner_id=?',$id, $this->f3->get('SESSION.userId')]);
        $this->erase();
    }
}