<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 28.08.2018
 * Time: 14:12
 */

/**
 * Class UserModel
 */
class UserModel extends DB\SQL\Mapper
{

    /**
     * UserModel constructor.
     * @param \DB\SQL $db
     */
    public function __construct(DB\SQL $db)
    {
        parent::__construct($db, 'user');
    }

    /**
     * Get user by name
     * @param $name
     * @return array
     */
    public function getByName($name)
    {
        $this->load(array('username=?', $name));
        return $this->query;
    }

    /**
     * Add user
     * @param $username
     * @param $passwordHash
     */
    public function add($username, $passwordHash)
    {
        $this->set('username', $username);
        $this->set('password', $passwordHash);
        $this->save();
    }
}